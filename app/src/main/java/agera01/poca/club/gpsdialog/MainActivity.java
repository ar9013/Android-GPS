package agera01.poca.club.gpsdialog;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


   static final int GpsPermission =1;
    private Button btn_Gps;
    static int count =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getGpsPermission();

        btn_Gps = (Button)findViewById(R.id.btn_Gps);
        btn_Gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it2Gps = new Intent(MainActivity.this,GPSActivity.class);
                startActivity(it2Gps);
            }
        });

    }


    private void getGpsPermission(){


        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},GpsPermission);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch(requestCode) {
            case GpsPermission:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //取得權限，進行檔案存取

                } else {
                    //使用者拒絕權限，停用檔案存取功能
                }
                return;
        }


    }
}
