package agera01.poca.club.gpsdialog;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.lang.annotation.Target;

import static agera01.poca.club.gpsdialog.MainActivity.GpsPermission;
import static agera01.poca.club.gpsdialog.MainActivity.count;

public class GPSActivity extends AppCompatActivity {


    boolean isGpsEnable;

    private static final int Gps = 2;
    private static final String TAG = "GPSActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        // Dialog Builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Start LBS AR ?").setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(),"Welecome to LBS AR.",Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        // 按下 否 回到 MainActivity
                        Intent it2MainActivity = new Intent(GPSActivity.this,MainActivity.class);
                        startActivity(it2MainActivity);
                    }
                });



       if(checkGPS()){

           Log.d(TAG,"checkGPS TRUE " + count);

           if(count <1){
               Log.d(TAG,"checking GPS ");

                // shoe Dialog
               AlertDialog alert = builder.create();
               alert.show();

                count =1;
           }
       }else{
           Log.d(TAG,"checkGPS FALSE");

           //跳轉到 Gps 設定頁面
           Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
           startActivityForResult(locationIntent, GpsPermission);



       }

    }

    private boolean checkGPS(){

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);




        return gps;
    }

}
